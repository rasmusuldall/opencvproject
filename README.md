## ANSIGTSGENKENDELSE I REALTID MED ANONYMISERINGSFUNKTION

Mit forsøg på at lave ansigtsgenkendelse i realtid med anonymiseringsfunktioner for at bevare privatliv og sikre overholdelse af persondataregulativer. Projektet er skrevet i C++ med hjælp fra OpenCV, som er et open source computervision og maskinlæringssoftwarebibliotek. Al kode er skrevet i en enkelt fil, main.cpp, og haarcascade_frontalface_default er inkluderet til debugging formål. Resterende HaarCascade Classifiers kan findes [her](https://github.com/opencv/opencv/tree/master/data/haarcascades).
