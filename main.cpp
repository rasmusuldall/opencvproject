#include <opencv2/opencv.hpp>

int main() {
    // Indlæs cascade classifier til frontal ansigtsgenkendelse
    cv::CascadeClassifier faceCascade;
    if (!faceCascade.load("C:/opencv/haarcascade_frontalface_default.xml")) {
        std::cerr << "Error loading face cascade." << std::endl;
        return -1;
    }

    // Åbn videooptagelsesobjektet (0 for default kamera)
    cv::VideoCapture capture(0);
    if (!capture.isOpened()) {
        std::cerr << "Error opening camera." << std::endl;
        return -1;
    }

    // Opret et vindue for at vise kamerafeedet
    cv::namedWindow("Face Detection", cv::WINDOW_NORMAL);

    // Boolean for at bestemme, om sløring skal anvendes
    bool applyBlur = true;

    while (true) {
        // Koden fanger frame i et loop, indtil der opstår en fejl eller loopet brydes manuelt
        cv::Mat frame;
        capture >> frame;
        if (frame.empty()) {
            std::cerr << "Error reading frame." << std::endl;
            break;
        }

        
        // Konverter til gråtoner til ansigtsgenkendelse     
        cv::Mat gray;
        cv::cvtColor(frame, gray, cv::COLOR_BGR2GRAY);

        // Reducer falske positiver ved at justere parametre
        std::vector<cv::Rect> faces;
        faceCascade.detectMultiScale(gray, faces, 1.3, 5, cv::CASCADE_SCALE_IMAGE, cv::Size(30, 30));

        // Loop der itererer over elementer i faces
        for (const auto& face : faces) {
            // Tegn rektangler omkring registrerede ansigter
            cv::rectangle(frame, face, cv::Scalar(0, 255, 0), 2);

            // Conditional blok, der anvender Gaussisk sløring på et Region of Interest (ROI) under en bestemt tilstand (boolean)
            if (applyBlur) {
                cv::Mat faceROI = frame(face);
                cv::GaussianBlur(faceROI, faceROI, cv::Size(75, 75), 0);
            }
        }

        // Vis sløringsstatus (Anonymize) i højre hjørne som enten on eller off
        std::string blurStatus = "Anonymize: " + std::string(applyBlur ? "On" : "Off");
        cv::putText(frame, blurStatus, cv::Point(frame.cols - 150, 25), cv::FONT_HERSHEY_COMPLEX, 0.5, cv::Scalar(255, 255, 255), 1);

        // Denne funktion bruges til at vise et billede i et eksisterende vindue
        cv::imshow("Face Detection", frame);

        // Bryd loopet, hvis brugeren trykker på 'Esc'-tasten
        int key = cv::waitKey(1);
        if (key == 27) {
            break;
        }
        // Slå anvend sløring til/fra, når brugeren trykker på 'a'
        else if (key == 'a') {
            applyBlur = !applyBlur;
        }
    }

    // Afslut videooptagelsesobjektet
    capture.release();

    // Luk vinduet
    cv::destroyAllWindows();

    return 0;
}
